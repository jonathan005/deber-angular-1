import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-primera-seccion',
  templateUrl: './primera-seccion.component.html',
  styleUrls: ['./primera-seccion.component.scss']
})
export class PrimeraSeccionComponent implements OnInit {
  form: FormGroup;
  mensaje: string = '';
  
  constructor() {
    this.form = new FormGroup({
      palabra: new FormControl("",Validators.required)});
    
  }


  ngOnInit() {
  }

  isPalindromo() {
    let str:string = this.form.get('palabra').value;
    str = str.toUpperCase();
    if(str.split('').reverse().join('') === str){
      this.mensaje = "Es Palíndromo";
    }
    else{
      this.mensaje = "No es Palíndromo";
    }
    console.log(this.mensaje);
  }


}
